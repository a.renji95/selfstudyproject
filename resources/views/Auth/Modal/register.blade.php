<div id="register-form" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h2>
                                Register Form
                            </h2>
                            <p>
                                Please follow the order below
                            </p>
                        <form id="regiser-form" action="#" class="wizard-big">
                            {{ csrf_field() }}
                            <h1>Account</h1>
                            <fieldset>
                                <h2>Account Information</h2>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Email <span style="color: red">*</span></label>
                                            <input id="register_email" name="register_email" type="text"
                                                   class="form-control required" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label>Password <span style="color: red">*</span></label>
                                            <input id="register_password" name="register_password" type="password"
                                                   class="form-control required" placeholder="Password">
                                        </div>
                                        <div class="form-group">
                                            <label>Confirm Password <span style="color: red">*</span></label>
                                            <input id="register_confirm_password" name="register_confirm_password"
                                                   type="password"
                                                   class="form-control required" placeholder="Confirm password">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <h1>Profile</h1>
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input id="register_name" name="register_name" type="text" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Gender</label>
                                            <div class="radio radio-info radio-inline register">
                                                <input type="radio" id="inlineRadio1" value="0" name="register_gender" checked="">
                                                <label for="inlineRadio1">Male</label>
                                            </div>
                                            <div class="radio radio-inline register">
                                                <input type="radio" id="inlineRadio2" value="1" name="register_gender">
                                                <label for="inlineRadio2">Female</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Phone</label>
                                            <input id="register_phone" name="register_phone" type="text"
                                                   class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input id="email" name="email" type="text"
                                                   class="form-control required email">
                                        </div>
                                        <div class="form-group">
                                            <label>Address *</label>
                                            <input id="address" name="address" type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <h1>Warning</h1>
                            <fieldset>
                                <div class="text-center" style="margin-top: 120px">
                                    <h2>You did it Man :-)</h2>
                                </div>
                            </fieldset>

                            <h1>Finish</h1>
                            <fieldset>
                                <h2>Terms and Conditions</h2>
                                <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> <label
                                        for="acceptTerms">I agree with the Terms and Conditions.</label>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


