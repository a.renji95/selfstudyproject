<!DOCTYPE html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HiepNH | Đăng Nhập </title>
    <base href="{{asset('')}}">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/plugins/bootstrapSocial/bootstrap-social.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
</head>

<body class="gray-bg onepiece-background">

<div class="loginColumns animated fadeInDown">
    <div class="row">

        <div class="col-md-6 login-content">
            <h1 class="font-bold">Welcome to HiepNH Website</h1>

            <p>
                Perfectly designed and precisely prepared admin theme with over 50 pages with extra new web app views.
            </p>

            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
            </p>

            <p>
                When an unknown printer took a galley of type and scrambled it to make a type specimen book.
            </p>

            <p>
                <small>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</small>
            </p>

        </div>
        <div class="col-md-6">
            <div class="ibox-content">
                <form class="m-t" role="form" method="POST" enctype="multipart/form-data" action="index.html" id="loginForm">
                    {{csrf_field()}}
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email" name="email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                    </div>
                    <button type="submit" class="btn btn-primary block full-width">Login</button>
                    <div class="m-b">
                        <a href="#">
                            <small>Forgot password?</small>
                        </a>
                    </div>
                    <a class="btn btn-block btn-social btn-facebook" >
                        <span class="fa fa-facebook"></span> Sign in with Facebook
                    </a>
                    <a class="btn btn-block btn-social btn-google" >
                        <span class="fa fa-google"></span> Sign in with Google
                    </a>
                    <a class="btn btn-block btn-social btn-instagram" >
                        <span class="fa fa-instagram"></span> Sign in with Instagram
                    </a>
                    <a class="btn btn-block btn-social btn-twitter" >
                        <span class="fa fa-twitter"></span> Sign in with Twitter
                    </a>
                    <p class="text-muted text-center">
                        <small>Do not have an account?</small>
                    </p>
                    <a data-toggle="modal" class="btn btn-sm btn-white btn-block" href="#register-form">Create an account</a>
                </form>
                <p class="m-t">
                    <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small>
                </p>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row login-content">
        <div class="col-md-6">
            <b>START CODE DATE</b>
        </div>
        <div class="col-md-6 text-right">
            <small><b>© 28/01/2019</b></small>
        </div>
    </div>
    @include('Auth.Modal.register')
</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/validation/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="js/inspinia.js"></script>
<script src="js/plugins/pace/pace.min.js"></script>

<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.min.js"></script>
<script type="text/javascript" language="javascript">
    $().ready(function () {
        $("#loginForm").validate({
            rules: {
                "email": {
                    required: true,
                    validateEmail: true,
                    maxlength: 50
                },
                "password": {
                    required: true
                }
            },
            messages: {
                "email": {
                    required: 'Please enter your email!!',
                    email: 'Email example: example@outlook.com',
                    maxlength: $.validator.format("Email contains {0} or less than characters!!"),
                },
                "password": {
                    required: 'Please enter your password!!'
                }
            }
        });

        $.validator.addMethod("validateEmail", function (value, element) {
            return this.optional(element) || /^[a-zA-Z]+([a-zA-Z0-9_.])*@[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/.test(value);
        }, "Email example: example@outlook.com");

        $.validator.addMethod("validatePassword", function (value, element) {
            return this.optional(element) || /^(?!.*[\s])(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@$#%^&*]).+$/.test(value);
        }, "Password: up,lowercase,number,special char,not space");

        $("#regiser-form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex) {
                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex) {
                    return true;
                }
                // Forbid suppressing "Warning" step if the user is to young
                if (newIndex === 3 && Number($("#age").val()) < 18) {
                    return false;
                }
                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex) {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex) {
                // Suppress (skip) "Warning" step if the user is old enough.
                if (currentIndex === 2 && Number($("#age").val()) >= 18) {
                    $(this).steps("next");
                }

                // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                if (currentIndex === 2 && priorIndex === 3) {
                    $(this).steps("previous");
                }
            },
            onFinishing: function (event, currentIndex) {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex) {
                var form = $(this);

                // Submit form input
                form.submit();
            }
        });
        //     .validate({
        //     errorPlacement: function (error, element)
        //     {
        //         element.before(error);
        //     },
        //     rules: {
        //         "register_email": {
        //             required: true,
        //             validateEmail: true,
        //             maxlength: 50
        //         },
        //         "register_password": {
        //             required: true,
        //             minlength: 10,
        //             maxlength: 20,
        //             validatePassword: true
        //         },
        //         "register_confirm_password": {
        //             required: true,
        //             equalTo: "#register_password"
        //         }
        //     },
        //     messages: {
        //         "register_email": {
        //             required: 'Please enter your email!!',
        //             email: 'Email example: example@outlook.com',
        //             maxlength: $.validator.format("Email contains {0} or less than characters!!"),
        //         },
        //         "register_password": {
        //             required: 'Please enter your password!!',
        //             minlength: $.validator.format("Password must contain at least {0} characters!!"),
        //             maxlength: $.validator.format("Password must be less than {0} characters!!")
        //         },
        //         "register_confirm_password": {
        //             required: 'Please confirm your password!!',
        //             equalTo: "Password confirm not match"
        //         }
        //     }
        // });
    });
</script>
</body>
</html>
