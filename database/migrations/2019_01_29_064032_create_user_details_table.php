<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->string('name', 50)->nullable();
            $table->tinyInteger('gender')->nullable();
            $table->string('address',50)->nullable();
            $table->string('district', 50)->nullable();
            $table->string('province_city', 50)->nullable();
            $table->string('phone_number', 50)->nullable();
            $table->dateTime('birthday')->nullable();
            $table->string('image_link', 255)->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
